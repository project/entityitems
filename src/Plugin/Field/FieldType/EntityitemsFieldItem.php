<?php

namespace Drupal\entityitems\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Defines the 'entityitems_field' field type.
 *
 * @FieldType(
 *   id = "entityitems_field",
 *   label = @Translation("Entity items"),
 *   category = @Translation("General"),
 *   default_widget = "entityitems_widget",
 *   default_formatter = "entityitems_formatter"
 * )
 */
class EntityitemsFieldItem extends FieldItemBase {

  const KEY_TITLE = 'title';
  const KEY_SUMMARY = 'summary';
  const KEY_MARKUP = 'markup';
  const KEY_MARKUP_FORMAT = 'markup_format';
  const KEY_MARKUP_PROCESSED = 'markup_processed';
  const KEY_URI = 'uri';
  const KEY_ENTITY_ID = 'entity_id';
  const KEY_MEDIA_ID = 'media_id';
  const KEY_VARIANT = 'variant';

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'backend' => 'geofield_backend_default',
      ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'backend' => 'geofield_backend_default',
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        self::KEY_TITLE => [
          'type' => 'varchar_ascii',
          'default' => '',
          'length' => 255,
          'not null' => FALSE,
        ],
        self::KEY_SUMMARY => [
          'type' => 'varchar_ascii',
          'default' => '',
          'length' => 4096,
          'not null' => FALSE,
        ],
        self::KEY_MARKUP => [
          'type' => $field_definition->getSetting('case_sensitive') ? 'blob' : 'text',
          'size' => 'big',
        ],
        self::KEY_URI => [
          'type' => 'varchar_ascii',
          'default' => '',
          'length' => 2048,
          'not null' => FALSE,
        ],
        self::KEY_ENTITY_ID => [
          'description' => 'The ID of the target entity.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        self::KEY_MEDIA_ID => [
          'description' => 'The ID of the media entity.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        self::KEY_VARIANT => [
          'description' => 'The ID of the media entity.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
      ],
      'indexes' => [
        'entity_id' => ['entity_id'],
        'media_id' => ['media_id'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties[self::KEY_TITLE] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Subtitle'))
      ->setRequired(FALSE);

    $properties[self::KEY_SUMMARY] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Summary'))
      ->setRequired(FALSE);

    $properties[self::KEY_MARKUP] = DataDefinition::create('string')
      ->setLabel(t('Markup'))
      ->setRequired(TRUE);

    $properties[self::KEY_MARKUP_FORMAT] = DataDefinition::create('filter_format')
      ->setLabel(t('Markup format'));

    $properties[self::KEY_MARKUP_PROCESSED] = DataDefinition::create('string')
      ->setLabel(t('Processed markup'))
      ->setDescription(t('Markup with the format applied.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\text\TextProcessed')
      ->setSetting('text source', 'value')
      ->setInternal(FALSE);

    $properties[self::KEY_URI] = DataDefinition::create('uri')
      ->setLabel(new TranslatableMarkup('URL'))
      ->setRequired(FALSE);

    $properties[self::KEY_ENTITY_ID] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Entity reference'))
      ->setSetting('unsigned', TRUE);

    $properties[self::KEY_MEDIA_ID] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Media reference'))
      ->setSetting('unsigned', TRUE);

    $properties[self::KEY_VARIANT] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Variant'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element['markup'] = [
      '#type' => 'markup',
      '#value' => 'this is the storage class',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    parent::setValue($values);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCache() {
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $value = [
      self::KEY_TITLE => 'A sample thing',
      self::KEY_SUMMARY => '',
      self::KEY_MARKUP => '',
      self::KEY_URI => '',
      self::KEY_ENTITY_ID => NULL,
      self::KEY_MEDIA_ID => NULL,
      self::KEY_VARIANT => '',
    ];
    return $value;
  }

}
