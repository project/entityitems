<?php

namespace Drupal\entityitems\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entityitems\Plugin\Field\FieldType\EntityitemsFieldItem as FieldItem;

/**
 * Defines the 'entityitems_widget' field widget.
 *
 * @FieldWidget(
 *   id = "entityitems_widget",
 *   label = @Translation("Entity items widget"),
 *   field_types = {"entityitems_field"},
 * )
 */
class EntityitemsFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'foo' => 'bar',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['foo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Foo'),
      '#default_value' => $this->getSetting('foo'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Foo: @foo', ['@foo' => $this->getSetting('foo')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $settings = $this->getFieldSettings();

    $element[FieldItem::KEY_TITLE] = [
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->{FieldItem::KEY_TITLE},
    ];
    $element[FieldItem::KEY_SUMMARY] = [
      '#title' => $this->t('Summary'),
      '#type' => 'textarea',
      '#rows' => 5,
      '#default_value' => $items[$delta]->{FieldItem::KEY_SUMMARY},
    ];
    $element[FieldItem::KEY_MARKUP] = [
      '#title' => $this->t('Body'),
      '#type' => 'text_format',
      '#format' => '',
      '#default_value' => $items[$delta]->{FieldItem::KEY_MARKUP},
      '#rows' => 5,
      //'#placeholder' => $this->getSetting('placeholder'),
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
    ];
    $element[FieldItem::KEY_URI] = [
      '#title' => $this->t('Uri'),
      '#type' => 'url',
      '#size' => $this->getSetting('size'),
      '#default_value' => $items[$delta]->{FieldItem::KEY_URI},
    ];

    //    $element[FieldItem::KEY_ENTITY_ID] = [
//    ];
//    $element[FieldItem::KEY_MEDIA_ID] = [
//    ];
//    $element[FieldItem::KEY_VARIANT] = [
//    ];

    return $element;
  }

}
